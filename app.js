
const config = require('./config');
const express = require('express');
const expressSession  = require('express-session');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const ioSharedSession = require('express-socket.io-session');
const mysql = require('./lib/Database')(config);
const passport = require('passport');
const Strategy = require('passport-discord').Strategy;

/**
 * Express View Configuration
 */
app.use(express.static('public'));
app.set('view engine', 'ejs');

/**
 * Express Discord Session
 */
const guildRoutes = require('./guildRoutes');

const guildRouteById = (id) => {
    for (const [route, guild] of Object.entries(guildRoutes)) {
        if (guild.id === id) {
            return route;
        }
    }

    return null;
};

const authMiddleware = (req, res, next) => {
    const guildPath = req.path ? req.path.replace('/history', '') : '';

    if (typeof guildRoutes[guildPath] !== 'undefined') {
        req.session.guild = guildRoutes[guildPath];
    }

    if (req.isAuthenticated()) {
        return next();
    }

    res.redirect('/login');
}

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((obj, done) => {
    done(null, obj);
});

passport.use(new Strategy({
    clientID: config.discord.clientId,
    clientSecret: config.discord.clientSecret,
    callbackURL: `${config.url}api/discord/callback`,
    scope: ['identify', 'guilds'],
}, (accessToken, refreshToken, profile, done) => {
    process.nextTick(() => done(null, profile));
}));

const session = expressSession({
    secret: 'keyboard dog',
    resave: false,
    saveUninitialized: false,
});
app.use(session);

app.use(passport.initialize());
app.use(passport.session());

app.get('/api/discord', passport.authenticate('discord', { scope: ['identify', 'guilds'] }), (req, res) => {});

app.get('/api/discord/callback', passport.authenticate('discord', { failureRedirect: '/api/discord' }), (req, res) => {
    // Auth success
    res.redirect('/');
});

app.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

/**
 * Express Routes
 */
app.get('/', (req, res) => {
    if (typeof req.session.guild !== 'undefined') {
        res.redirect(guildRouteById(req.session.guild.id));
        return;
    }

    // TODO render a view instead
    res.send('You must visit the server specific URL!');
});

app.get('/login', (req, res) => {
    res.render('login', {
        url: config.url,
        user: {},
        guild: req.session.guild,
        action: '/api/discord',
    });
});

for (const guildRoute of Object.keys(guildRoutes)) {
    app.get(guildRoute, authMiddleware, (req, res) => {
        req.session.historyMode = false;

        res.render('index', {
            url: config.url,
            route: guildRoute,
            user: req.user,
            guild: req.session.guild,
        });
    });

    app.get(`${guildRoute}/history`, (req, res) => {
        req.session.historyMode = true;
        req.session.guild = guildRoutes[guildRoute];

        res.render('history', {
            url: config.url,
            user: {},
            guild: req.session.guild,
        });
    });
}

/**
 * Storage
 */
const storage = {
    users: {},
    filledTiles: {},
};

// Add guild ids to user and tile storage for scope filtering
for (const guild of Object.values(guildRoutes)) {
    storage.users[guild.id] = {};
    storage.filledTiles[guild.id] = {};
}

/**
 * Sockets
 */
io.use(ioSharedSession(session, { autoSave: true }));

io.on('connection', async (socket) => {
    /**
     * Session data
     */
    const session = socket.handshake.session;
    const historyMode = session.historyMode || false;

    // Do nothing if not logged in yet unless history mode
    if ((!socket.handshake.session.passport || !socket.handshake.session.passport.user) && !historyMode) {
        return;
    }

    const discordUser = session.passport ? session.passport.user : null;
    const discordGuild = session.guild;

    /**
     * Initial step, create the user
     */
    // Check if user is in the appropriate guild
    const userDiscordGuild = discordUser ? discordUser.guilds.find((guild) => guild.id === discordGuild.id) : null;

    // Check if user has already joined
    const createNewUser = !historyMode && typeof storage.users[discordGuild.id][discordUser.id] === 'undefined';

    if (typeof userDiscordGuild === 'undefined') {
        io.to(socket.id).emit('not guild member', discordGuild);
        socket.disconnect();
        return;
    }

    if (createNewUser) {
        storage.users[discordGuild.id][discordUser.id] = {
            id: discordUser.id,
            name: discordUser.username,
            avatar: discordUser.avatar,
        };
    }

    io.emit('user connect', discordGuild.id, storage.users[discordGuild.id]);

    /**
     * Update filled tile data and emit to current user
     */
    const emitFilledTiles = async () => {
        if (!discordGuild) {
            return;
        }

        if (!Object.values(storage.filledTiles[discordGuild.id]).length) {
            const db = await mysql.get();
            const [rows, fields] = await db.execute(`
                SELECT col, row, user, color
                FROM filled_tiles
                WHERE guild = ?
                ORDER BY created_at DESC
            `, [discordGuild.id]);

            const newFilledTiles = {};

            for (const filledTile of rows) {
                const tileKey = `${filledTile.col}:${filledTile.row}`;

                if (typeof newFilledTiles[tileKey] === 'undefined') {
                    newFilledTiles[tileKey] = filledTile;
                }
            }

            // If no rows found then create a blank item to render the grid on FE
            if (!Object.values(newFilledTiles).length) {
                newFilledTiles['0:0'] = {
                    col: 0, row: 0, user: '', color: '#fff',
                };
            }

            storage.filledTiles[discordGuild.id] = newFilledTiles;
        }

        io.emit('filled tiles', discordGuild.id, Object.values(storage.filledTiles[discordGuild.id]));
    };

    socket.on('filled tiles', emitFilledTiles);

    /**
     * For rendering history
     */
    socket.on('history data', async () => {
        const db = await mysql.get();
        const [countRows, coundFields] = await db.execute(`
            SELECT count(id) as count
            FROM filled_tiles
            WHERE guild = ?
        `, [discordGuild.id]);
        const totalCount = countRows[0].count;
        const perPage = Math.ceil(totalCount * 0.02); // 2% per page
        const totalPages = Math.ceil(totalCount / perPage);

        let page = 0;
        const fetchPagesLoop = async () => {
            const [rows, fields] = await db.execute(`
                SELECT col, row, user, color
                FROM filled_tiles
                WHERE guild = ?
                ORDER BY created_at ASC
                LIMIT ${page * perPage}, ${perPage}
            `, [discordGuild.id]);

            io.to(socket.id).emit('history data', rows, {
                page: page + 1,
                perPage, totalPages, totalCount,
            });

            page++;
            if (page < totalPages) {
                setTimeout(fetchPagesLoop, 200);
            }
        }
        fetchPagesLoop();
    });

    /**
     * User has left
     */
    socket.on('disconnect', () => {
        if (!discordUser) {
            return;
        }

        if (storage.users[discordGuild.id][discordUser.id]) {
            // Emit user leaving
            io.emit('user disconnect', discordGuild.id, storage.users[discordGuild.id]);

            // Remove the user
            delete storage.users[discordGuild.id][discordUser.id];
        }
    });

    /**
     * Fill the tile
     */
    socket.on('fill tile', async (filledTile) => {
        filledTile.user = discordUser.username;

        // Save the tile to database
        const db = await mysql.get();
        await db.execute('INSERT INTO `filled_tiles` SET `guild` = ?, `col` = ?, `row` = ?, `color` = ?, `user` = ?, `created_at` = NOW()', [
            discordGuild.id,
            filledTile.col,
            filledTile.row,
            filledTile.color,
            filledTile.user,
        ]);

        // Update tile storage in a way that it replaces the old col:row tile
        storage.filledTiles[discordGuild.id][`${filledTile.col}:${filledTile.row}`] = filledTile;

        // Send the tile update
        io.emit('fill tile', discordGuild.id, filledTile);
    });
});

http.listen(config.port, () => {
    console.log(`listening on *:${config.port}`);
});
