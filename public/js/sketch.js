
let loadingTiles = false;
let tileMap;
let mapColCount;
let mapRowCount;

const hoverOverlayTile = { active: false, col: 0, row: 0, user: '' };
let mobileTileConfirmed = false;

const tileSize = 6;
const tileStrokeWeight = 0.2;

const drawGrid = false;

let userColor = $('#colors a').first().addClass('active').css('background-color');

const createRange = (from, to) => {
    const numbers = [];

    for (let i = from; i < to; i++) {
        numbers.push(i);
    }

    return numbers;
};

const createMap = (colCount, rowCount) => {
    const tileMap = {};

    for (let col = 0; col < colCount; col++) {
        tileMap[col] = {};

        for (let row = 0; row < rowCount; row++) {
            tileMap[col][row] = {
                col,
                row,
                color: '#fff',
                filled: false,
                user: '',
            };
        }
    }

    return tileMap;
};

function setup() {
    noLoop();
    createCanvas(1200, 600);

    mapColCount = Math.floor(height / tileSize);
    mapRowCount = Math.floor(width / tileSize);
    tileMap = createMap(mapColCount, mapRowCount);

    canvasQueue.ready();
}

function draw(settings = {}) {
    clear();
    background(255);

    if (settings.clearTileMap) {
        tileMap = createMap(mapColCount, mapRowCount);
    }

    // Update the tiles that are filled
    if (!filledTiles.length) {
        if (!loadingTiles) {
            socket.emit('filled tiles');
            loadingTiles = true;
        }

        text('Loading XD', width / 2, height / 2);
        return;
    }

    for (const filledTile of filledTiles) {
        const tile = tileMap[filledTile.col][filledTile.row];

        if (tile) {
            tile.filled = true;
            tile.color = filledTile.color;
            tile.user = filledTile.user;

            tileMap[filledTile.col][filledTile.row] = tile;
        }
    }

    // Draw the tile map
    for (const col of createRange(0, mapColCount)) {
        for (const [row, tile] of Object.entries(tileMap[col])) {
            const tileX = tile.row * tileSize - tileStrokeWeight / 2;
            const tileY = tile.col * tileSize - tileStrokeWeight / 2;

            // Only draw if it's filled
            if (tile.filled) {
                fill(tile.color);
                noStroke();
                strokeWeight(tileStrokeWeight);
                rect(tileX, tileY, tileSize, tileSize);
            }
        }
    }

    // Draw the grid
    if (drawGrid) {
        for (const col of createRange(0, mapColCount)) {
            const tileY = col * tileSize - tileStrokeWeight / 2;

            stroke('#ddd');
            strokeWeight(tileStrokeWeight);
            line(0, tileY, width, tileY);
        }

        for (const row of createRange(0, mapRowCount)) {
            const tileX = row * tileSize - tileStrokeWeight / 2;

            stroke(86, 125);
            strokeWeight(tileStrokeWeight);
            line(tileX, 0, tileX, height);
        }
    }

    // Hover overlay tile and user name pop-up
    if (hoverOverlayTile.active) {
        const tile = hoverOverlayTile;

        const tileX = tile.row * tileSize;
        const tileY = tile.col * tileSize;

        fill(userColor);
        noStroke();
        rect(tileX, tileY, tileSize, tileSize);

        // Handle the pop-up with HTML so it can go outside the canvas
        if (hoverOverlayTile.user) {
            $('#tile-info').show().html(`
                By: ${hoverOverlayTile.user}
            `).css({
                top: tileY + tileSize + $('.p5Canvas').offset().top,
                left: tileX + tileSize / 2 + $('.p5Canvas').offset().left,
            });
        } else {
            $('#tile-info').hide();
        }
    } else {
        $('#tile-info').hide();
    }
}

function mouseMoved() {
    const col = Math.floor(mouseY / tileSize);
    const row = Math.floor(mouseX / tileSize);

    if (isMobile() || !canDraw) {
        return;
    }

    try {
        // Call the tilemap object within try to validate that a tile in current col, row exists
        const tile = tileMap[col][row];

        hoverOverlayTile.col = tile.col;
        hoverOverlayTile.row = tile.row;
        hoverOverlayTile.user = tile.user;
        hoverOverlayTile.active = true;

        draw();
    } catch (e) {
        // Tile not found, disable
        hoverOverlayTile.active = false;
    }
}

function mousePressed(e) {
    // Only if the LMB is pressed
    if (e.which !== 1) {
        return;
    }

    let col = Math.floor(mouseY / tileSize);
    let row = Math.floor(mouseX / tileSize);

    if (isMobile() && e.target.id === 'mobile-tile-confirm') {
        mobileTileConfirmed = true;

        col = hoverOverlayTile.col;
        row = hoverOverlayTile.row;
    }

    if (!canDraw) {
        return;
    }

    try {
        // Set color
        const tile = tileMap[col][row];

        // Add a hover tile instead if not confirmed on mobile yet
        if (isMobile() && !mobileTileConfirmed) {
            hoverOverlayTile.col = col;
            hoverOverlayTile.row = row;
            hoverOverlayTile.user = tile.user;
            hoverOverlayTile.active = true;

            $('#mobile-tile-confirm').show();

            draw();
            return;
        } else {
            hoverOverlayTile.active = false;

            $('#mobile-tile-confirm').hide();

            // Reset the confirmation
            mobileTileConfirmed = false;
        }

        // Do nothing if the tile already is the same color
        if (tile.color === userColor) {
            return;
        }

        const filledTile = {
            col,
            row,
            color: userColor,
            user: USER.username,
        };

        socket.emit('fill tile', filledTile);

        // Client side tile update
        //filledTiles.push(filledTile);

        draw();
    } catch (e) {
        // Tile not found, do nothing
    }
}

$(document).on('click', '#colors a', (e) => {
    $('#colors a').removeClass('active');
    userColor = $(e.target).addClass('active').css('background-color');
});
